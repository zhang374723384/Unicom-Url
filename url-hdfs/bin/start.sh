#!/bin/sh

[  -e `dirname $0`/env.sh ]  && . `dirname $0`/env.sh

if [ ! -d "$LOG_DIR" ] ;then
    mkdir "$LOG_DIR"
    if [ $? -ne 0 ] ;then
        echo "Cannot create $LOG_DIR" >&2
        exit 1
    fi
fi

if [ -f $PID_FILE ]
then
 PID=`cat $PID_FILE`
 ps -ef | awk '{print $2}' | grep "^$PID$" >/dev/null
 if [ $? -eq 0 ]
 then
  echo "process $PID in running."
  exit 1
 fi
fi
echo 'start run'
FIRSTFILE="`dirname $0`/first_result.txt"
MIDFILE="`dirname $0`/middle_result.txt"
RESFILE="`dirname $0`/result_table.txt"
echo 'run $FIRSTFILE'
hive -f $FIRSTFILE
#echo 'run $MIDFILE'
#hive -f $MIDFILE
#echo 'run com.unicom.batch.BatchTiao'
#$JAVA_PATH/java $JAVA_OPTS -cp "$CONFIG_PATH:$CLASS_PATH" com.unicom.batch.BatchTiao
#echo 'run com.unicom.batch.BatchPBOC'
#$JAVA_PATH/java $JAVA_OPTS -cp "$CONFIG_PATH:$CLASS_PATH" com.unicom.batch.BatchPBOC
#echo 'run $RESFILE'
#hive -f $RESFILE
#echo 'run com.unicom.batch.BatchUploadFtp'
#$JAVA_PATH/java $JAVA_OPTS -cp "$CONFIG_PATH:$CLASS_PATH" com.unicom.batch.BatchUploadFtp
echo $! > $PID_FILE
PID=`cat $PID_FILE `
echo "process $PID is start ok."

