package unicom.utils;


import org.slf4j.LoggerFactory;
import unicom.domain.Record;

import java.sql.*;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：mysql jdbc工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class MysqlUtil {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(MysqlUtil.class);
    private static Connection conn;
    //驱动程序名
    private static String driver = "com.mysql.jdbc.Driver";
    //URL指向要访问的数据库名mydata
    private static String url = ConfigUtil.getString("datacenter.unicom.mysql.url");
    //MySQL配置时的用户名
    private static String user = ConfigUtil.getString("datacenter.unicom.mysql.username");
    //MySQL配置时的密码
    private static String password = ConfigUtil.getString("datacenter.unicom.mysql.password");

    static {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            logger.error("获取mysql链接失败:{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：mysql添加ftp文件下载记录方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static void toAddRecord(Record record) {
        try {
            PreparedStatement preparedStatement = MysqlUtil.conn.prepareStatement("INSERT INTO down_record (startTime,endTime,ftpPath,localPath,fileSize,operateNo,speed,elapsedTime) values (?,?,?,?,?,?,?,?)");
            preparedStatement.setTimestamp(1, new Timestamp(record.getStartTime().getTime()));
            preparedStatement.setTimestamp(2, new Timestamp(record.getEndTime().getTime()));
            preparedStatement.setString(3, record.getFtpPath());
            preparedStatement.setString(4, record.getLocalPath());
            preparedStatement.setString(5, record.getFileSize());
            preparedStatement.setString(6, record.getOperateNo());
            preparedStatement.setString(7, record.getSpeed());
            preparedStatement.setString(8, record.getElapsedTime());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("向mysql添加下载记录失败:{}", e);
        }
    }

    public static void main(String[] args) {
//        try {
//            Statement statement = MysqlUtil.conn.createStatement();
//            String sql = "select * from down_record";
//            //3.ResultSet类，用来存放获取的结果集！！
//            ResultSet rs = statement.executeQuery(sql);
//            while(rs.next()){
//                //获取stuname这列数据
//                System.out.println(rs.getString(3));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }
}
