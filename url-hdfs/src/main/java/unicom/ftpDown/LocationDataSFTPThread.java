package unicom.ftpDown;

import com.jcraft.jsch.ChannelSftp;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.serializer.StringEncoder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import unicom.utils.ConfigUtil;
import unicom.utils.HiveJDBCUtil;
import unicom.utils.SftpUtil;

import java.io.*;
import java.net.InetAddress;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：主类连接ftp下载数据并发送kafka
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class LocationDataSFTPThread implements Runnable {
    private static Logger logger = LoggerFactory.getLogger(LocationDataSFTPThread.class);
    private static HiveJDBCUtil hiveJDBCUtil = new HiveJDBCUtil();
    private static Date initDate = new Date();//初始化时间
    //获取kafka生产者对象
    private static Producer kafkaProducer = getKafkaProducer();

    private String localStr;

    public LocationDataSFTPThread(String localStr) {
        this.localStr = localStr;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：主程序执行入口方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void run() {
        while (true) {
            start(this.localStr);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：该线程url清洗逻辑方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static void start(String localStr) {
        try {
            //遍历该线程对应本地存储文件夹文件解压读取并过滤数据
            File root = new File(ConfigUtil.getString("datacenter.unicom.localpath") + "/" + localStr);
            if (!root.exists()) {
                root.mkdirs();
            }
            showAllFiles(root, localStr, kafkaProducer);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("主程序执行入口方法失败,Method:{},error:{}", "start", e);
        }
    }

    public static void showAllFiles(File dir, String localStr, Producer kafkaProducer) {
        try {
            File[] fs = dir.listFiles();
            for (int i = 0; i < fs.length; i++) {
                String absolutePath = fs[i].getAbsolutePath();
                if (fs[i].isDirectory()) {
                    try {
                        showAllFiles(fs[i], localStr, kafkaProducer);
                    } catch (Exception e) {
                    }
                } else {
                    //截取处理文件在ftp的目录加入发送信息
                    String ftpFilePath = absolutePath.replace(ConfigUtil.getString("datacenter.unicom.localpath") + "/" + localStr, "");
                    //下载文件到本地并发送kafka，同時刪除ftp上的数据
                    logger.info("Thread-" + Thread.currentThread().getId() + ",文件路径:" + absolutePath);
                    sendToKafka(ftpFilePath, fs[i], kafkaProducer, localStr);
                }
            }

            //写数据文件到hive
            getFilesToHive(ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + localStr, localStr);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("遍历本地文件失败,Method:{},error:{}", "showAllFiles", e);
        }

    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：下载文件到本地并发送kafka，同時刪除ftp上的数据
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void sendToKafka(String localFilePath, File file, Producer kafkaProducer, String localStr) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file)), "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                boolean flag = false;//是否过滤标识位
                String urlStr = line + "|" + localFilePath;
                String[] splitStr = urlStr.split("\\|");

                //url数据中有空字段的数据不予处理
                if (splitStr.length == 15) {
                    if (!splitStr[13].trim().equals("") && !splitStr[2].trim().equals("") && splitStr[4].contains(" ")) {
                        flag = true;
                        // 正则提取网址
                        String url = splitStr[13];
                        String regexUrl = getRegexUrl(url);
                        urlStr = urlStr.replace(url, regexUrl);
                    }
                }

                if (flag) {
                    //定时任务用于将处理完的数据加载进相应hive表分区中
                    Date operaDate = new Date();
                    if (((operaDate.getTime() - initDate.getTime()) / 1000) > Integer.parseInt(ConfigUtil.getString("datacenter.unicom.messageTiming"))) {
                        //写数据文件到hive
                        getFilesToHive(ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + localStr, localStr);
                        //初始化定时任务时间
                        initDate = operaDate;
                    }

                    //发送消息到kafka
                    KeyedMessage<String, String> km = new KeyedMessage<String, String>(ConfigUtil.getString("datacenter.unicom.kafkatopic"), urlStr);
                    kafkaProducer.send(km);

                    String[] split = urlStr.split("\\|");
                    String urlFtpFilePath = split[14];//url ftp路径
                    String startTime = split[4];
                    String urlStrLocal = urlStr.substring(0, urlStr.lastIndexOf("|"));
                    //获取分区所需的字段名
                    String source = ConfigUtil.getString("datacenter.unicom.source");
                    String province = urlFtpFilePath.substring(0, urlFtpFilePath.lastIndexOf("/")).substring(
                            urlFtpFilePath.substring(0, urlFtpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
                    urlFtpFilePath = urlFtpFilePath.substring(0, urlFtpFilePath.lastIndexOf("/"));
                    String dateTime = urlFtpFilePath.substring(0, urlFtpFilePath.lastIndexOf("/")).substring(
                            urlFtpFilePath.substring(0, urlFtpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
                    //获取操作的小时
                    Calendar cal = getCalendar(startTime, "yyyy-MM-dd hh:mm:ss");
                    String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));

                    cal = getCalendar(dateTime, "yyyyMMdd");
                    String year = String.valueOf(cal.get(Calendar.YEAR));
                    String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
                    String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

                    //本地该日省份url写入文件
                    String partition = getPartition(year, month, day, hour, province, source);
                    String fileDir = ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + localStr + "/" + partition;
                    File localProvicenDir = new File(fileDir);
                    if (!localProvicenDir.exists()) {
                        localProvicenDir.mkdirs();
                    }

                    //写记录到本地文件
                    File localprovince = new File(localProvicenDir.getAbsolutePath() + "/url-" + localStr + ".data");
                    if (!localprovince.exists()) {
                        InetAddress netAddress = InetAddress.getLocalHost();
                        localprovince.createNewFile();
                        toWriteLocal(localprovince, urlStrLocal + "\n");
                    } else {
                        toWriteLocal(localprovince, urlStrLocal + "\n");
                    }
                }
            }
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下载文件到本地并发送kafka失败,Method:{},error:{}", "sendToKafka", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：url正则截取方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static String getRegexUrl(String url) {
        Pattern pattern = Pattern
                .compile("(http://|ftp://|https://|www|){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)");
        // 空格结束
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return url;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取hive的分区
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static String getPartition(String year, String month, String day, String hour, String province, String source) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(year + "/");
        stringBuffer.append(month + "/");
        stringBuffer.append(day + "/");
        stringBuffer.append(hour + "/");
        stringBuffer.append(province + "/");
        stringBuffer.append(source);
        return stringBuffer.toString();
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：日期格式化方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static Calendar getCalendar(String day, String formatStr) {
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
            Date parse = simpleDateFormat.parse(day);
            cal.setTime(parse);
        } catch (Exception e) {
            logger.error("日期格式化方法错误,error:{}", e);
        }
        return cal;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：将本地数据文件加载进hive表中
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void getFilesToHive(String filePath, String dir) {
        File root = new File(filePath);
        File[] files = root.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                getFilesToHive(file.getAbsolutePath(), dir);
            } else {
                String[] substring = filePath.replace(ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + dir + "/", "").split("/");
                hiveJDBCUtil.executeHql("alter table unicom_url add if not exists partition (year='" + substring[0] + "',month='" + substring[1] + "',day='" + substring[2] + "',hour='" + substring[3] + "',province='" + substring[4] + "',source='" + substring[5] + "')");
                toWriteHdfs(new File(file.getAbsolutePath()), substring);
            }
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：写文件到hdfs
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void toWriteHdfs(File localprovince, String[] substring) {
        try {
            logger.info("Thread-" + Thread.currentThread().getId() + ",localprovince:" + localprovince);
            InputStream in = new BufferedInputStream(new FileInputStream(localprovince));
            Path path = new Path(ConfigUtil.getString("datacenter.unicom.HdfsPath") + "/" + "year=" + substring[0] + "/month=" + substring[1] + "/day=" + substring[2] + "/hour=" + substring[3] + "/province=" + substring[4] + "/source=" + substring[5] + "/" + "url-" + UUID.randomUUID().toString().replaceAll("-", "") + ".data");
            Configuration conf = new Configuration();
            FileSystem fileSystem = FileSystem.get(new URI(
                    ConfigUtil.getString("datacenter.unicom.hdfs")), conf);
            FSDataOutputStream out = fileSystem.create(path);
            IOUtils.copyBytes(in, out, 4096, true);
            localprovince.delete();
        } catch (Exception e) {
            logger.error("[Class:UrlEtlBolt][Method:toWriteHdfs][Message:{}][Exception:{}]",
                    "本地文件写入hdfs错误", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取url数据写入本地路径
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void toWriteLocal(File file, String conent) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file, true)));
            out.write(conent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                logger.error("[Class:UrlEtlBolt][Method:toWriteLocal][Message:{}][Exception:{}]",
                        "过滤数据写入本地文件错误", e);
            }
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取kafka生产者
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static Producer getKafkaProducer() {
        Properties properties = new Properties();
        properties.put("zookeeper.connect", ConfigUtil.getString("datacenter.unicom.zookper"));//声明zk
        properties.put("serializer.class", StringEncoder.class.getName());
        properties.put("metadata.broker.list", ConfigUtil.getString("datacenter.unicom.kafkahost"));// 声明kafka broker
        return new Producer<String, String>(new ProducerConfig(properties));
    }
}
