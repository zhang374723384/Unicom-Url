package unicom.start;

import com.jcraft.jsch.ChannelSftp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import unicom.utils.ConfigUtil;
import unicom.utils.SftpUtil;

import java.util.Iterator;
import java.util.Vector;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月11日
 * 开发内容：ftp文件下载线程类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class FtpDownLoadThread {
    private static Logger logger = LoggerFactory.getLogger(FtpDownLoadThread.class);
    private static SftpUtil sftpUtil = new SftpUtil();

    public static void main(String[] args) {
        while (true) {
            //读取配置项ftp及kafka
            String host = ConfigUtil.getString("datacenter.unicom.ftphost");
            String port = ConfigUtil.getString("datacenter.unicom.port");
            String user = ConfigUtil.getString("datacenter.unicom.ftpuser");
            String password = ConfigUtil.getString("datacenter.unicom.ftppwd");
            String ftppath = ConfigUtil.getString("datacenter.unicom.ftppath");

            ChannelSftp sftp = sftpUtil.connect(host, Integer.parseInt(port), user, password);
            try {
                Vector ls = sftp.ls(ftppath);
                Iterator it = ls.iterator();
                while (it.hasNext()) {
                    String perfectInfor = it.next().toString();
                    String fileName = perfectInfor.split(" ")[perfectInfor.split(" ").length - 1];
                    //过滤掉文件夹名为.及..的文件夹
                    if (perfectInfor.substring(0, 1).equals("d") && !fileName.equals(".") && !fileName.equals("..")) {
                        //文件夹继续遍历
                        //读取ftp路径的文件并下载到本地文件夹
                        sftpUtil.readPathToQueue(ftppath + "/" + fileName, sftp);
                        sftp.disconnect();
                        Thread.sleep(120000);
                    }
                }
            } catch (Exception e) {
                logger.error("ftp下载启动类执行错误,{}", e);
            }
        }
    }
}
