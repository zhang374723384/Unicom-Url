package unicom.start;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import unicom.ftpDown.LocationDataSFTPThread;
import unicom.utils.ConfigUtil;
import unicom.utils.GlobalQueue;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：ftp文件下载系统启动类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class EnablementThread {
    private static Logger logger = LoggerFactory.getLogger(EnablementThread.class);
    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(ConfigUtil.getInt("datacenter.unicom.thread.num"), ConfigUtil.getInt("datacenter.unicom.thread.num"), 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(ConfigUtil.getInt("datacenter.unicom.thread.num")));

    public static void main(String[] args) {
        logger.info("The main class is starting!");
        //根据配置文件线程数启动相应数量线程
        for (int i = 0; i < ConfigUtil.getInt("datacenter.unicom.thread.num"); i++) {
            String localStr = GlobalQueue.arrayBlockingQueue.poll().toString();
            logger.info("The thread work is creating!");
            LocationDataSFTPThread locationDataSFTPThread = new LocationDataSFTPThread(localStr);
            executor.execute(locationDataSFTPThread);
            logger.info("The thread work is created!");
        }
    }
}
