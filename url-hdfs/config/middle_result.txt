﻿
DROP TABLE IF EXISTS dw_temp.adcl160120_bsnrl;
create table dw_temp.adcl160120_bsnrl as
select b.*  from
      (select id_acct,
              max(time_start) as time_start from dsc_dwd.dwd_icore_business_rl group by id_acct)a
       left join dsc_dwd.dwd_icore_business_rl b on a.id_acct=b.id_acct and a.time_start=b.time_start;


DROP TABLE IF EXISTS dw_temp.adcl160120_bsnunrl;
create table dw_temp.adcl160120_bsnunrl as
select b.*  from
      (select id_acct,
              max(time_start) as time_start from dsc_dwd.dwd_icore_business_url group by id_acct)a
       left join dsc_dwd.dwd_icore_business_url b on a.id_acct=b.id_acct and a.time_start=b.time_start;




DROP TABLE IF EXISTS dw_temp.adcl160120_acct;
create table dw_temp.adcl160120_acct as
select *
         from dw_temp.adcl160120_bsnrl;


DROP TABLE IF EXISTS dw_temp.adcl160120_varrslt;
create table dw_temp.adcl160120_varrslt as
select h.*,
       case when numr_pdpd>numr_pmaxdpd then numr_pdpd else numr_pmaxdpd end as num_dpd,
       case when num_draw=0 then 0 else (amt_draw/num_draw)/amt_curcl end as rat_utl1m,
       case when amt_draw=0 then 0 else (amt_repay/amt_draw) end as rat_rp1m
 from
(select a.id_acct as id_acct,a.id_unqp as id_unqp,
        no_org,
        name,
        type_acct,
        no_contr,acct_code,date_setup,amt_curcl,
        case when numr_pdpd is null then 0 else numr_pdpd end as numr_pdpd,
        case when numr_pmaxdpd is null then 0 else numr_pmaxdpd end as numr_pmaxdpd,
        case when num_draw is null then 0 else num_draw end as  num_draw,
        case when amt_draw is null then 0 else amt_draw end as  amt_draw,
        case when amt_repay is null then 0 else amt_repay end as amt_repay
       from dw_temp.adcl160120_acct a
       left join
(select id_unqp,
        case when max(NUM_DPD) is null then 0 else max(NUM_DPD) end as numr_pdpd,
        case when max(NUM_MAXDPD) is null then 0 else max(NUM_MAXDPD) end as numr_pmaxdpd
        from dw_temp.adcl160120_bsnrl group by id_unqp)b on a.id_unqp=b.id_unqp
       left join
(select ID_ACCT,
        count(*) as num_draw,
        sum(AMT_ORDER) as amt_draw
          from (select * from
                  (select a.*,b.DATE_SETUP
                         from dsc_dwd.dwd_icore_order_rl a left join dw_temp.adcl160120_bsnrl b on a.id_acct=b.id_acct
                         where a.TYPE_ORDER="withdraw")c
                         where  c.TIME_ORDER>=c.DATE_SETUP and c.TIME_ORDER<add_months(c.DATE_SETUP,1))d
        group by d.id_acct)e on a.id_acct=e.id_acct
       left join
(select id_acct,
        sum(amt_order) as amt_repay
        from (select * from
                  (select a.*,b.DATE_SETUP
                         from dsc_dwd.dwd_icore_order_rl a left join dw_temp.adcl160120_bsnrl b on a.id_acct=b.id_acct
                         where a.TYPE_ORDER="repay")c
                         where  c.TIME_ORDER>=c.DATE_SETUP and c.TIME_ORDER<add_months(c.DATE_SETUP,1))f
        group by f.id_acct)g on a.id_acct=g.id_acct

)h;
alter table dw_temp.adcl160120_varrslt add columns(tel_cell string);





DROP TABLE IF EXISTS dw_temp.adcl160120_scdcpacct;
create table dw_temp.adcl160120_scdcpacct as
select * from dw_temp.adcl160120_acct where
acct_code="000421";





DROP TABLE IF EXISTS dw_temp.adcl160120_basicinf;
create table dw_temp.adcl160120_basicinf as
select e.id_acct as id_acct,e.id_unqp as id_unqp,e.id_unqf as id_unqf,e.time_rect as time_rect,e.amt_curcl as amt_curcl
from
(select id_unqp,max(time_rect) as time_rect
        from
           (select a.id_acct,b.*
           from dw_temp.adcl160120_scdcpacct a
           join dsc_dws_adcl.dws_e_dsc_gaa01_person_basicinf  b
           on a.id_unqp=b.id_unqp)c group by c.id_unqp)d
 left join
           (select a.id_acct,a.amt_curcl,b.*
            from dw_temp.adcl160120_scdcpacct a
            join dsc_dws_adcl.dws_e_dsc_gaa01_person_basicinf  b
            on a.id_unqp=b.id_unqp)e on d.id_unqp=e.id_unqp and d.time_rect=e.time_rect;



DROP TABLE IF EXISTS dw_temp.adcl160120_varinqdet;
create table dw_temp.adcl160120_varinqdet as
select id_unqf,sum(case when rsn_inq in ("贷款审批","信用卡审批","本人查询")
                               and (YEAR(time_rect)-YEAR(timei))* 365 + (MONTH(time_rect)- MONTH(timei))*30 + DAY(time_rect)- DAY(timei) < 181
                          then 1
                          else 0 end) as num_hdiq6m
       from (select b.*,a.time_rect as time_rect from  dw_temp.adcl160120_basicinf a
            join dsc_dws_adcl.dws_e_dsc_gaa01_inq_detail b
            on a.id_unqf=b.id_unqf)c group by c.id_unqf;




DROP TABLE IF EXISTS dw_temp.adcl160120_varpddet;
create table dw_temp.adcl160120_varpddet as
select id_unqf,
       case when max(case when catg="准贷记卡" then num_dqm-1
                          else num_dqm end) is null then 0
            else max(case when catg="准贷记卡" then num_dqm-1
                          else num_dqm end) end as num_pd,
       case when max(case when (YEAR(time_rect)-YEAR(time_dq))* 12 + MONTH(time_rect)- MONTH(time_dq)<6 then
                          (case when catg="准贷记卡" then num_dqm-1
                          else num_dqm end)
                         else 0 end) is null then 0
            else max(case when (YEAR(time_rect)-YEAR(time_dq))* 12 + MONTH(time_rect)- MONTH(time_dq)<6 then
                           (case when catg="准贷记卡" then num_dqm-1
                            else num_dqm end)
                         else 0 end)end as num_pd6m
      from
           (select b.*,a.time_rect as time_rect from  dw_temp.adcl160120_basicinf a
            join dsc_dws_adcl.dws_e_dsc_gaa01_passdue_detail b
            on a.id_unqf=b.id_unqf)c group by c.id_unqf;



DROP TABLE IF EXISTS dw_temp.adcl160120_varloandet;
create table dw_temp.adcl160120_varloandet as
select id_unqf,
       count(nol_tra) as numl_loan,
       sum(case when typel like '%房%' then 1 else 0 end) as numl_mrtg,
       case when max(NUML_DRP) is null then 0 else  max(NUML_DRP) end as numl_maxcurpd,
       max(case when (YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)>=6 then 0
           else(case when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"G")>0 then 10
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"Z")>0 then 9
                when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"D")>0 then 8
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"7")>0 then 7
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"6")>0 then 6
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"5")>0 then 5
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"4")>0 then 4
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"3")>0 then 3
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"2")>0 then 2
             when
          INSTR(substring(rcdl_rp24m,
                      19+(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq),
                       6-(YEAR(time_rect)-YEAR(timel_lstinq))* 12 + MONTH(time_rect)- MONTH(timel_lstinq)),"1")>0 then 1
             else 0 end)end) as numl_wst6m,
       max(case when INSTR(rcdl_rp24m,"G")>0 then 10
                when INSTR(rcdl_rp24m,"Z")>0 then 9
                when INSTR(rcdl_rp24m,"D")>0 then 8
                when INSTR(rcdl_rp24m,"7")>0 then 7
                when INSTR(rcdl_rp24m,"6")>0 then 6
                when INSTR(rcdl_rp24m,"5")>0 then 5
                when INSTR(rcdl_rp24m,"4")>0 then 4
                when INSTR(rcdl_rp24m,"3")>0 then 3
                when INSTR(rcdl_rp24m,"2")>0 then 2
                when INSTR(rcdl_rp24m,"1")>0 then 1
           else 0 end) as numl_wst24m,
       max(case when stul_acct like '%呆账%' then 10
                else 0 end) as numl_wststu
       from  (select b.*,a.time_rect as time_rect from  dw_temp.adcl160120_basicinf a
              join dsc_dws_adcl.dws_e_dsc_gaa01_loan_l_detail b
              on a.id_unqf=b.id_unqf)c group by c.id_unqf;




DROP TABLE IF EXISTS dw_temp.adcl160120_varcredet;
create table dw_temp.adcl160120_varcredet as
select id_unqf,
       count(noc_tra) as numc_crecd,
       case when max(numc_drp) is null then 0 else max(numc_drp) end as numc_curpd,
       case when max(case when stuc_acct!="未激活" then (YEAR(time_rect)-YEAR(timec_iss))* 365 + (MONTH(time_rect)- MONTH(timec_iss))*30+DAY(time_rect)-DAY(timec_iss)
                     else 0 end)>=730  then 1
             else 0 end as flagc_fistcc2y,
       max(case when (YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)>=6 then 0
           else(case when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"G")>0 then 10
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"Z")>0 then 9
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"D")>0 then 8
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"7")>0 then 7
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"6")>0 then 6
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"5")>0 then 5
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"4")>0 then 4
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"3")>0 then 3
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"2")>0 then 2
             when
       INSTR(substring(rcdc_rp24m,
                      19+(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq),
                       6-(YEAR(time_rect)-YEAR(timec_lstinq))* 12 + MONTH(time_rect)- MONTH(timec_lstinq)),"1")>0 then 1
             else 0 end)end) as numc_wst6m,
       max(case when INSTR(rcdc_rp24m,"G")>0 then 10
                when INSTR(rcdc_rp24m,"Z")>0 then 9
                when INSTR(rcdc_rp24m,"D")>0 then 8
                when INSTR(rcdc_rp24m,"7")>0 then 7
                when INSTR(rcdc_rp24m,"6")>0 then 6
                when INSTR(rcdc_rp24m,"5")>0 then 5
                when INSTR(rcdc_rp24m,"4")>0 then 4
                when INSTR(rcdc_rp24m,"3")>0 then 3
                when INSTR(rcdc_rp24m,"2")>0 then 2
                when INSTR(rcdc_rp24m,"1")>0 then 1
           else 0 end) as numc_wst24m,
       max(case when stuc_acct like '%呆账%' then 10
                else 0 end) as numc_wststu,
       case when avg(case when stuc_acct is null or stuc_acct in("正常","逾期") then amtc_cl
                     else null end) is null then 0
                     else avg(case when stuc_acct is null or stuc_acct in("正常","逾期") then amtc_cl
                     else null end) end as amtc_avgacl,
       sum(case when stuc_acct in ("正常","冻结","止付") then AMTC_SHAR else 0 end) as amtc_sumacl,
       sum(case when stuc_acct in ("正常","冻结","止付") then  AMTC_AUTL6M else 0 end) as amtc_sumautl,
       sum(case when stuc_acct in ("正常","冻结","止付") then 1 else 0 end) as numc_acc
       from
          (select b.*,a.time_rect as time_rect from  dw_temp.adcl160120_basicinf a
           join dsc_dws_adcl.dws_e_dsc_gaa01_loan_c_detail b
           on a.id_unqf=b.id_unqf)c group by c.id_unqf;





DROP TABLE IF EXISTS dw_temp.adcl160120_varsmcredet;
create table dw_temp.adcl160120_varsmcredet as
select id_unqf,
        count(nos_tra) as nums_crecd,
        max(case when (YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)>=6 then 0
           else(case when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"G")>0 then 9
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"Z")>0 then 8
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"D")>0 then 7
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"7")>0 then 6
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"6")>0 then 5
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"5")>0 then 4
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"4")>0 then 3
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"3")>0 then 2
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"2")>0 then 1
             when
        INSTR(substring(rcds_rp24m,
                      19+(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq),
                       6-(YEAR(time_rect)-YEAR(times_lstinq))* 12 + MONTH(time_rect)- MONTH(times_lstinq)),"1")>0 then 0
             else 0 end)end) as nums_wst6m,
        max(case when INSTR(rcds_rp24m,"G")>0 then 9
                when INSTR(rcds_rp24m,"Z")>0 then 8
                when INSTR(rcds_rp24m,"D")>0 then 7
                when INSTR(rcds_rp24m,"7")>0 then 6
                when INSTR(rcds_rp24m,"6")>0 then 5
                when INSTR(rcds_rp24m,"5")>0 then 4
                when INSTR(rcds_rp24m,"4")>0 then 3
                when INSTR(rcds_rp24m,"3")>0 then 2
                when INSTR(rcds_rp24m,"2")>0 then 1
                when INSTR(rcds_rp24m,"1")>0 then 0
           else 0 end) as nums_wst24m,
       sum(case when stus_acct in ("正常","冻结","止付") then  AMTS_SHAR else 0 end) as amts_sumacl,
       sum(case when stus_acct in ("正常","冻结","止付") then  AMTS_AOD6M else 0 end) as amts_sumautl,
       max(case when stus_acct like '%呆账%' then 10
                else 0 end) as nums_wststu,
       sum(case when stus_acct in ("正常","冻结","止付") then 1 else 0 end)as nums_ascc
    from
       (select b.*,a.time_rect as time_rect from  dw_temp.adcl160120_basicinf a
        join dsc_dws_adcl.dws_e_dsc_gaa01_loan_s_detail b
        on a.id_unqf=b.id_unqf)c group by c.id_unqf;



DROP TABLE IF EXISTS dw_temp.adcl160120_rsltpboc;
create table dw_temp.adcl160120_rsltpboc as
select j.*,
       round(case when typesnd_acct="N" then 0 else(case when typesnd_acct="A" then 500
                                             else(case when typesnd_acct="B" then 1000
                                                       else(case when typesnd_acct="C" then (case when 50000<
                                                                          (case when amtc_avgacl*0.6*rat_adjstcl>6000 then amtc_avgacl*0.6*rat_adjstcl
                                                                                     else 6000 end) then 50000 else
                                                                          (case when amtc_avgacl*0.6*rat_adjstcl>6000 then amtc_avgacl*0.6*rat_adjstcl
                                                                                     else 6000 end)end)
                                                                 else (case when 50000<
                                                                          (case when amtc_avgacl*0.8*rat_adjstcl>15000 then amtc_avgacl*0.8*rat_adjstcl
                                                                                     else 15000 end) then 50000 else
                                                                          (case when amtc_avgacl*0.8*rat_adjstcl>15000 then amtc_avgacl*0.8*rat_adjstcl
                                                                                     else 15000 end) end)end)end)end)end,-2) as amt_tmpcl,
     case when num_wstrat>=3 then "rjadcl01" else(case when  num_wstrat6m>=2 then "rjadcl02"
                                                     else(case when num_hdiq6m>=5 then "rjadcl03"
                                                           else(case when num_curpd>0 then "rjadcl04"
                                                                     else(case when rat_adjstcl=0 then "rjadcl05" else "none"end)end)end)end)end as rsn_rj

from
(select i.*,
      case when num_wstrat>=3 or num_wstrat6m>=2 or num_hdiq6m>=5 or num_curpd>0 or rat_adjstcl=0  then "N"
                else (case when flag_hasfile=0 then "A"
                       else (case when flagc_fistcc2y=0 then "B"
                                 else(case when numl_mrtg=0 then "C" else "D" end)end)end)end as typesnd_acct

  from
(select h.*,
        case when rat_clusd>=0.7 then 0
                       else(case when rat_clusd>=0.6 then 0.8
                                 else(case when rat_clusd>=0.5 then 0.9
                                           else 1 end)end)end as rat_adjstcl
  from
(select g.*,
        case when numl_loan+numc_crecd+nums_crecd>0 then 1 else 0 end as flag_hasfile,
           case when numl_wststu+numc_wststu+nums_wststu>0 then 10
            else (case when num_pd>
                        (case when numl_wst24m>
                                              (case when numc_wst24m>nums_wst24m then numc_wst24m else nums_wst24m end)
                         then numl_wst24m else (case when numc_wst24m>nums_wst24m then numc_wst24m else nums_wst24m end) end)
                 then num_pd else (case when numl_wst24m>
                                              (case when numc_wst24m>nums_wst24m then numc_wst24m else nums_wst24m end)
                             then numl_wst24m else (case when numc_wst24m>nums_wst24m then numc_wst24m else nums_wst24m end) end)
                 end)end as num_wstrat,
        case when num_pd6m>
                        (case when numl_wst6m>
                                              (case when numc_wst6m>nums_wst6m then numc_wst6m else nums_wst6m end)
                         then numl_wst6m else (case when numc_wst6m>nums_wst6m then numc_wst6m else nums_wst6m end) end)
            then num_pd6m else (case when numl_wst6m>
                                              (case when numc_wst6m>nums_wst6m then numc_wst6m else nums_wst6m end)
                          then numl_wst6m else (case when numc_wst6m>nums_wst6m then numc_wst6m else nums_wst6m end) end)
            end as num_wstrat6m,
     case when numl_maxcurpd>numc_curpd then numl_maxcurpd else numc_curpd end as num_curpd,
     case when nums_ascc+numc_acc=0 then 0 else(case when amtc_sumacl+amts_sumacl=0 then 1
                                                     else (amtc_sumautl+amts_sumautl)/(amtc_sumacl+amts_sumacl) end)end as rat_clusd
 from
(select a.*,
        case when num_hdiq6m     is null then 0 else num_hdiq6m     end as num_hdiq6m,
        case when num_pd         is null then 0 else num_pd         end as num_pd,
        case when num_pd6m       is null then 0 else num_pd6m       end as num_pd6m,
        case when numl_loan      is null then 0 else numl_loan      end as numl_loan,
        case when numl_mrtg      is null then 0 else numl_mrtg      end as numl_mrtg,
        case when numl_maxcurpd  is null then 0 else numl_maxcurpd  end as numl_maxcurpd,
        case when numl_wst6m     is null then 0 else numl_wst6m     end as numl_wst6m,
        case when numl_wst24m    is null then 0 else numl_wst24m    end as numl_wst24m,
        case when numl_wststu    is null then 0 else numl_wststu    end as numl_wststu,
        case when numc_crecd     is null then 0 else numc_crecd     end as numc_crecd,
        case when numc_curpd     is null then 0 else numc_curpd     end as numc_curpd,
        case when flagc_fistcc2y is null then 0 else flagc_fistcc2y end as flagc_fistcc2y,
        case when numc_wst6m     is null then 0 else numc_wst6m     end as numc_wst6m,
        case when numc_wst24m    is null then 0 else numc_wst24m    end as numc_wst24m,
        case when numc_wststu    is null then 0 else numc_wststu    end as numc_wststu,
        case when amtc_avgacl    is null then 0 else amtc_avgacl    end as amtc_avgacl,
        case when amtc_sumacl    is null then 0 else amtc_sumacl    end as amtc_sumacl,
        case when amtc_sumautl   is null then 0 else amtc_sumautl   end as amtc_sumautl,
        case when numc_acc       is null then 0 else numc_acc       end as numc_acc,
        case when nums_crecd     is null then 0 else nums_crecd     end as nums_crecd,
        case when nums_wst6m     is null then 0 else nums_wst6m     end as nums_wst6m,
        case when nums_wst24m    is null then 0 else nums_wst24m    end as nums_wst24m,
        case when amts_sumacl    is null then 0 else amts_sumacl    end as amts_sumacl,
        case when amts_sumautl   is null then 0 else amts_sumautl   end as amts_sumautl,
        case when nums_wststu    is null then 0 else nums_wststu    end as nums_wststu,
        case when nums_ascc      is null then 0 else nums_ascc      end as nums_ascc
from  dw_temp.adcl160120_basicinf a left join dw_temp.adcl160120_varinqdet   b on a.id_unqf=b.id_unqf
                                    left join dw_temp.adcl160120_varpddet    c on a.id_unqf=c.id_unqf
                                    left join dw_temp.adcl160120_varloandet  d on a.id_unqf=d.id_unqf
                                    left join dw_temp.adcl160120_varcredet   e on a.id_unqf=e.id_unqf
                                    left join dw_temp.adcl160120_varsmcredet f on a.id_unqf=f.id_unqf)g)h)i)j;




DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01p2p;
create table dw_temp.adcl160120_rsltbl01p2p as
select id_unqp,count(PK_P2PBLC) as num_p2p
         from
       (select y.id_unqp,y.PK_P2PBLC
        from dw_temp.adcl160120_acct x join
        dsc_dws_adcl.dws_e_dsc_eac01_blc y on x.id_unqp=y.id_unqp where flag_blc!=0)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01wcsh;
create table dw_temp.adcl160120_rsltbl01wcsh as
select id_unqp,count(PK_WECASHBLC) as num_wcsh
        from
       (select y.id_unqp ,y.PK_WECASHBLC
        from dw_temp.adcl160120_acct x join
        dsc_dws_adcl.dws_e_dsc_eaf01_blc y on x.id_unqp=y.id_unqp)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01lhp;
create table dw_temp.adcl160120_rsltbl01lhp as
select id_unqp,count(ID_UNQF) as num_lhp
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_eae01_blc y on x.id_unqp=y.id_unqp where stu_mr!=0)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01hf;
create table dw_temp.adcl160120_rsltbl01hf as
select id_unqp,count(ID_UNQF) as num_hf
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_eag01_basic y on x.id_unqp=y.id_unqp)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01nc1;
create table dw_temp.adcl160120_rsltbl01nc1 as
select id_unqp,count(ID_UNQF) as num_nc01
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
            dsc_dws_adcl.dws_e_dsc_gab01_imr y on x.id_unqp=y.id_unqp where (stu_imr!="一致" or stu_nmr!="一致" or stu_id!="有效状态"))t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01nc2;
create table dw_temp.adcl160120_rsltbl01nc2 as
select id_unqp,count(ID_UNQF) as num_nc02
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
            dsc_dws_adcl.dws_e_dsc_gab01_bbl_detail y on x.id_unqp=y.id_unqp where (stu_bl0!="BL00" and stu_bl0 is not null) or (stu_bl1!="不存在于BL1类数据库中" and stu_bl1 is not null))t
           group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01nc3;
create table dw_temp.adcl160120_rsltbl01nc3 as
select id_unqp,count(PK_NCIICINC) as num_nc03
        from
          (select y.id_unqp,y.PK_NCIICINC
          from dw_temp.adcl160120_acct x join
            dsc_dws_adcl.dws_e_dsc_gab01_inc y on x.id_unqp=y.id_unqp)t group by t.id_unqp;


DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl01;
create table dw_temp.adcl160120_rsltbl01 as
select  z.*,
       case when num_p2p+num_wcsh+num_lhp+num_hf+num_nc01+num_nc02+num_nc03>0 then 1 else 0 end as flag_bl01

from
(select a.*,
       case when num_p2p     is null then 0 else num_p2p end as num_p2p,
       case when num_wcsh    is null then 0 else num_wcsh end as num_wcsh,
       case when num_lhp     is null then 0 else num_lhp end as num_lhp,
       case when num_hf      is null then 0 else num_hf end as num_hf,
       case when num_nc01    is null then 0 else num_nc01 end as num_nc01,
       case when num_nc02    is null then 0 else num_nc02 end as num_nc02,
       case when num_nc03    is null then 0 else num_nc03 end as num_nc03

 from dw_temp.adcl160120_acct a left join  dw_temp.adcl160120_rsltbl01p2p  b on a.id_unqp=b.id_unqp
                                left join  dw_temp.adcl160120_rsltbl01wcsh c on a.id_unqp=c.id_unqp
                                left join  dw_temp.adcl160120_rsltbl01lhp d on a.id_unqp=d.id_unqp
								left join  dw_temp.adcl160120_rsltbl01hf e on a.id_unqp=e.id_unqp
								left join  dw_temp.adcl160120_rsltbl01nc1 f on a.id_unqp=f.id_unqp
								left join  dw_temp.adcl160120_rsltbl01nc2 g on a.id_unqp=g.id_unqp
								left join  dw_temp.adcl160120_rsltbl01nc3 h on a.id_unqp=h.id_unqp

)z;

DROP TABLE IF EXISTS dw_temp.adcl160120_acctrslt;
create table dw_temp.adcl160120_acctrslt as
select  e.*,
        case when  flag_adcl="n" then NULL
                  else (case when typesnd_acct="C" or typesnd_acct="D" then amt_tmpcl
                             else(case when rat_utl1m>0 and  rat_utl1m<=0.7  and rat_rp1m>0 and  rat_rp1m<=0.7 then amt_curcl*2
                                       else (case when rat_utl1m>0 and  rat_utl1m<=0.7 and rat_rp1m>0.7 then amt_curcl*2.5
                                                  else (case when rat_utl1m>0.7  and rat_rp1m>0 and rat_rp1m<=0.7 then amt_curcl*1.5
                                                            else(case when rat_utl1m>0.7   and rat_rp1m>0.7 then amt_curcl*3
                                                                      else NULL end)end)end)end)end)end as creditlmt
from
(select
       c.*,
       case when  amt_draw=0 then "rjadcl06"
            else (case when num_dpd>0 or amt_repay=0 then "rjadcl07"
                      else (case when typesnd_acct="N" then rsn_rj
                                 else(case when flag_bl01=1 then "rjadcl08" else "none" end)end)end)end as rsn_rjadcl,

       case when acct_code="004101" then add_months(date_setup,12)
            else NULL end as expiredate,
       case when num_dpd>0 or flag_bl01=1 or typesnd_acct="N" or amt_draw=0 or amt_repay=0 then "n" else "y" end as flag_adcl
from
(select a.*,
        b.id_unqf,
        case when b.typesnd_acct is null then "NJS" else b.typesnd_acct end as typesnd_acct,
        case when amt_tmpcl is null then 0 else amt_tmpcl end as amt_tmpcl,
        case when rsn_rj is null then "NJS" else rsn_rj end as rsn_rj,
        flag_bl01
        from dw_temp.adcl160120_varrslt a
left join dw_temp.adcl160120_rsltpboc b on a.id_acct=b.id_acct
left join adcl160120_rsltbl01 d on a.id_acct=d.id_acct)c)e;

DROP TABLE IF EXISTS dw_temp.adcl160120_accttobl;
create table dw_temp.adcl160120_accttobl as
select id_acct,id_unqp,
       tel_cell
       from dw_temp.adcl160120_acctrslt where flag_adcl="y";