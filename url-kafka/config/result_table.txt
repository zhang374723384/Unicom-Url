
DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02p2p;
create table dw_temp.adcl160120_rsltbl02p2p as
select id_unqp,count(PK_P2PBLC) as num_p2p
         from
       (select y.id_unqp,y.PK_P2PBLC
        from dw_temp.adcl160120_acct x join
        dsc_dws_adcl.dws_e_dsc_eac01_blc y on x.id_unqp=y.id_unqp where flag_blc!=0)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02wcsh;
create table dw_temp.adcl160120_rsltbl02wcsh as
select id_unqp,count(PK_WECASHBLC) as num_wcsh
        from
       (select y.id_unqp ,y.PK_WECASHBLC
        from dw_temp.adcl160120_acct x join
        dsc_dws_adcl.dws_e_dsc_eaf01_blc y on x.id_unqp=y.id_unqp)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02lhp;
create table dw_temp.adcl160120_rsltbl02lhp as
select id_unqp,count(ID_UNQF) as num_lhp
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_eae01_blc y on x.id_unqp=y.id_unqp where stu_mr!=0)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02hf;
create table dw_temp.adcl160120_rsltbl02hf as
select id_unqp,count(ID_UNQF) as num_hf
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_eag01_basic y on x.id_unqp=y.id_unqp)t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02nc1;
create table dw_temp.adcl160120_rsltbl02nc1 as
select id_unqp,count(ID_UNQF) as num_nc01
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_gab01_imr y on x.id_unqp=y.id_unqp where (stu_imr!="一致" or stu_nmr!="一致" or stu_id!="有效状态"))t group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02nc2;
create table dw_temp.adcl160120_rsltbl02nc2 as
select id_unqp,count(ID_UNQF) as num_nc02
        from
          (select y.id_unqp,y.ID_UNQF
           from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_gab01_bbl_detail y on x.id_unqp=y.id_unqp where (stu_bl0!="BL00" and stu_bl0 is not null) or (stu_bl1!="不存在于BL1类数据库中" and stu_bl1 is not null))t
           group by t.id_unqp;

DROP TABLE IF EXISTS dw_temp.adcl160120_rsltbl02nc3;
create table dw_temp.adcl160120_rsltbl02nc3 as
select id_unqp,count(PK_NCIICINC) as num_nc03
        from
          (select y.id_unqp,y.PK_NCIICINC
          from dw_temp.adcl160120_acct x join
           dsc_dws_adcl.dws_e_dsc_gab01_inc y on x.id_unqp=y.id_unqp)t group by t.id_unqp;



DROP TABLE IF EXISTS dw_temp.adcl160120_rsltblout;
create table dw_temp.adcl160120_rsltblout as
select  z.*,
       case when num_p2p+num_wcsh+num_lhp+num_hf+num_nc01+num_nc02+num_nc03>0 then 1 else 0 end as flag_bl02

from
(select a.*,
       case when num_p2p     is null then 0 else num_p2p end as num_p2p,
       case when num_wcsh    is null then 0 else num_wcsh end as num_wcsh,
       case when num_lhp     is null then 0 else num_lhp end as num_lhp,
       case when num_hf      is null then 0 else num_hf end as num_hf,
       case when num_nc01    is null then 0 else num_nc01 end as num_nc01,
       case when num_nc02    is null then 0 else num_nc02 end as num_nc02,
       case when num_nc03    is null then 0 else num_nc03 end as num_nc03

 from dw_temp.adcl160120_acct a left join  dw_temp.adcl160120_rsltbl02p2p  b on a.id_unqp=b.id_unqp
                                left join  dw_temp.adcl160120_rsltbl02wcsh c on a.id_unqp=c.id_unqp
                                left join  dw_temp.adcl160120_rsltbl02lhp d on a.id_unqp=d.id_unqp
								left join  dw_temp.adcl160120_rsltbl02hf e on a.id_unqp=e.id_unqp
								left join  dw_temp.adcl160120_rsltbl02nc1 f on a.id_unqp=f.id_unqp
								left join  dw_temp.adcl160120_rsltbl02nc2 g on a.id_unqp=g.id_unqp
								left join  dw_temp.adcl160120_rsltbl02nc3 h on a.id_unqp=h.id_unqp

)z;


DROP TABLE IF EXISTS dw_temp.adcl160120_acctrslt01;
create table dw_temp.adcl160120_acctrslt01 as
select  e.*,
        case when  flag_adcl="n" then NULL else creditlmtor end as creditlmt,
        case when acct_code="004101" then "y" else "n" end as flag_adclexp,
        1 as rank_adcl,
        current_timestamp as time_inst
from
(select a.no_org as org,
       a.no_contr as contrnbr,
       a.id_acct as  acctnbr,
       a.type_acct as accttype,
       a.name as name,
       a.id_unqp as id_unqp,
       id_unqf,
       a.acct_code as acct_code,
       a.date_setup as date_setup,
       a.amt_curcl as amt_curcl,
       a.creditlmt as creditlmtor,
       case when b.flag_bl02=1 then "rjadcl08" else rsn_rjadcl end as rsn_rjadcl,
       expiredate,
       case when b.flag_bl02=1 then "n" else flag_adcl end as flag_adcl

 from
   dw_temp.adcl160120_acctrslt a
   left join dw_temp.adcl160120_rsltblout b on a.id_acct=b.id_acct)e;

DROP TABLE IF EXISTS dw_temp.adcl160120_filerslt;
create table dw_temp.adcl160120_filerslt as
select org,contrnbr,acctnbr,accttype,name,creditlmt,expiredate
from dw_temp.adcl160120_acctrslt01 where flag_adcl="y" or flag_adclexp="y";


Insert into  dsc_dwd.idwd_adcl160120_result
 select id_unqp,acctnbr,id_unqf, org, contrnbr, accttype,
        name, acct_code, date_setup, amt_curcl, creditlmt, expiredate, 1,rsn_rjadcl, flag_adcl, flag_adclexp,current_timestamp  from  dw_temp.adcl160120_acctrslt01;

