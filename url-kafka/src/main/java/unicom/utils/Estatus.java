package unicom.utils;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：定义状态枚举类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public enum Estatus {
    BLACK_FAULT("F"),
    BLACK_SUCCESS("S"),
    BLACK_SUCCESSCCODE("200");

    private String estatus;

    private Estatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    @Override
    public String toString() {
        return this.estatus.toString();
    }

}
