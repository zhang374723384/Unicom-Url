package unicom.utils;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：ftp文件地址的存储队列
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class GlobalQueue {
    public static String stormPath = "";
    public static ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(Integer.parseInt(ConfigUtil.getString("datacenter.unicom.queue.max.num")));

    public static String getStormPath() {
        return stormPath;
    }

    public static void setStormPath(String stormPath) {
        GlobalQueue.stormPath = stormPath;
    }
}
