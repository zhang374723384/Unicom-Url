package unicom.utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：配置文件读取类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class ConfigUtil {

    private static Logger logger = LoggerFactory.getLogger(ConfigUtil.class);

    private static Configuration conf = null;

    static {
        try {
            conf = new PropertiesConfiguration("config.properties");
            loadPropertiesFile("dbconfig.properties");
        } catch (ConfigurationException e) {
            logger.error("[Class:ConfigUtil][Exception:{}]", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：加载配置文件方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void loadPropertiesFile(String name) throws ConfigurationException {
        Configuration configuration = new PropertiesConfiguration(name);
        Iterator<String> keys = configuration.getKeys();
        while (keys.hasNext()) {
            String key = keys.next();
            conf.addProperty(key, configuration.getProperty(key));
        }
    }

    public static int getInt(String key) {
        return conf.getInt(key);
    }

    public static String getString(String key) {
        return conf.getString(key);
    }

}
