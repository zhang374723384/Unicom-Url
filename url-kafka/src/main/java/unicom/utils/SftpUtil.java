package unicom.utils;

import com.google.gson.Gson;
import com.jcraft.jsch.*;
import unicom.domain.Record;
import org.slf4j.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：sftp工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class SftpUtil {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(SftpUtil.class);
    private static Gson gson = new Gson();

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：初始化连接sftp服务器
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public ChannelSftp connect(String host, int port, String username,
                               String password) {
        ChannelSftp sftp = null;
        try {
            JSch jsch = new JSch();
            jsch.getSession(username, host, port);
            Session sshSession = jsch.getSession(username, host, port);
            logger.info("Session created.");
            sshSession.setPassword(password);
            Properties sshConfig = new Properties();
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            logger.info("Session connected.");
            logger.info("Opening Channel.");
            Channel channel = sshSession.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
            logger.info("Connected to " + host + ".");
        } catch (Exception e) {
            logger.error("连接sftp服务器失败:{}", e);
        }
        return sftp;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：ftp上传文件
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void upload(String directory, String uploadFile, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            File file = new File(uploadFile);
            sftp.put(new FileInputStream(file), file.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：下载文件方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void download(String directory, String downloadFile, String saveFile, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            File file = new File(saveFile);
            Date startDate = new Date();
            sftp.get(downloadFile, new FileOutputStream(file));
            Date endDate = new Date();
            String fileSize = String.valueOf(file.length());
            float elapsedTime = (float) (endDate.getTime() - startDate.getTime()) / 1000;
            String speed = String.valueOf((Integer.parseInt(fileSize) / elapsedTime) / 1024);

            //将下载记录存储Mysql
            Record record = new Record();
            record.setStartTime(startDate);
            record.setEndTime(endDate);
            record.setFtpPath(directory + "/" + downloadFile);
            record.setLocalPath(saveFile);
            record.setFileSize(fileSize);
            record.setOperateNo(ConfigUtil.getString("datacenter.unicom.cur.no"));
            record.setSpeed(speed);
            record.setElapsedTime(elapsedTime + "s");
            //添加下载记录
            MysqlUtil.toAddRecord(record);
            logger.info("下载远程ftp文件{}到{}成功，用时{}秒,速度{}byte/s", directory + "/" + downloadFile, saveFile, elapsedTime, speed);
        } catch (Exception e) {
            logger.error("从sftp下载文件失败:{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：删除文件方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void delete(String directory, String deleteFile, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            sftp.rm(deleteFile);
        } catch (Exception e) {
            logger.error("从sftp删除文件失败:{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：列出目录下的文件
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public Vector listFiles(String directory, ChannelSftp sftp) throws SftpException {
        return sftp.ls(directory);
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：读取ftp路径的文件路径加入队列
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void readPathToQueue(String ftppath, ChannelSftp sftp) {
        try {
            Vector fileVector = sftp.ls(ftppath);
            Iterator it = fileVector.iterator();
            while (it.hasNext()) {
                //获取文件或文件夹全信息解析，例：-rw-rw-r--    1 ggsftp   ggsftp     450560 Mar 24 10:29 CDDW004151115006603_0.gz
                String perfectInfor = it.next().toString();
                String fileName = perfectInfor.split(" ")[perfectInfor.split(" ").length - 1];
                //过滤掉文件夹名为.及..的文件夹
                if (perfectInfor.substring(0, 1).equals("d") && !fileName.equals(".") && !fileName.equals("..")) {
                    //文件夹继续遍历
                    readPathToQueue(ftppath + "/" + fileName, sftp);
                } else if (perfectInfor.substring(0, 1).equals("-")) {
                    if (GlobalQueue.arrayBlockingQueue.size() == Integer.parseInt(ConfigUtil.getString("datacenter.unicom.queue.max.num"))) {
                        break;
                    }
                    logger.info("Get file is " + ftppath + "/" + fileName);
                    //判断该文件是否在该机器上进行操作，分布式策略
                    if ((fileName.hashCode() % Integer.parseInt(ConfigUtil.getString("datacenter.unicom.count"))) == Integer.parseInt(ConfigUtil.getString("datacenter.unicom.cur.no"))) {
                        //获取文件大小
                        HashMap<String, String> strHashMap = new HashMap<String, String>();
                        strHashMap.put("fileName", ftppath + "/" + fileName);
                        //将文件信息组织成json格式存储
                        GlobalQueue.arrayBlockingQueue.add(gson.toJson(strHashMap));
                        logger.info("ArrayBlockingQueue add file:" + ftppath + "/" + fileName);
                    }
                }
            }
        } catch (SftpException e) {
            logger.error("读取ftp路径的文件路径加入队列失败:{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取文件大小
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private String getFileSize(String perfectInfor) {
        String regEx = "[0-9]{4,12}";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(perfectInfor);
        if (!matcher.find()) {
            logger.error("获取文件大小失败,未匹配到文件大小！");
            return Integer.MIN_VALUE + "";
        }
        return matcher.group(0);
    }


    public static void main(String[] args) {
        try {
            SftpUtil sf = new SftpUtil();
            String host = "61.50.252.5";
            int port = 22;
            String username = "ggsftp";
            String password = "bg_DATA_xidian";
            String directory = "/home/ggsftp/detailorder/downing";
            ChannelSftp sftp = sf.connect(host, port, username, password);
//            sf.delete("/upload/20160329/", "CDDW004151115006708_1.gz", sftp);
            Vector ls = sftp.ls(directory);
            System.out.println(sftp.pwd());
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }


}
