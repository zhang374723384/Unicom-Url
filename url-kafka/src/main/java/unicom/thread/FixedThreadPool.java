package unicom.thread;

import unicom.utils.ConfigUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：定义线程池类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class FixedThreadPool implements ThreadPool{
    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(ConfigUtil.getInt("datacenter.unicom.thread.num"));

    public void execute(Runnable runnable) {
        newFixedThreadPool.execute(runnable);
    }
}
