package unicom.thread;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：线程池接口类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public interface ThreadPool {
    void execute(Runnable runnable);
}
