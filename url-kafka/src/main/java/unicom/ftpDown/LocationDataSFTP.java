package unicom.ftpDown;

import com.google.gson.Gson;
import com.jcraft.jsch.ChannelSftp;
import unicom.thread.FixedThreadPool;
import unicom.thread.ThreadPool;
import unicom.utils.ConfigUtil;
import unicom.utils.DateUtil;
import unicom.utils.GlobalQueue;
import unicom.utils.SftpUtil;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.serializer.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：主类连接ftp下载数据并发送kafka
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class LocationDataSFTP {
    private static Logger logger = LoggerFactory.getLogger(LocationDataSFTP.class);
    private static SftpUtil sftpUtil = new SftpUtil();
    private static Gson gson = new Gson();
    private static ThreadPool threadPool = new FixedThreadPool();

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：主程序执行入口方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static void start(String fileName, ChannelSftp sftp) {
        try {
            //读取配置项ftp及kafka
            String ftppath = ConfigUtil.getString("datacenter.unicom.ftppath");
            String localpath = ConfigUtil.getString("datacenter.unicom.localpath");

            //获取kafka生产者对象
            Producer kafkaProducer = getKafkaProducer();

            //读取ftp路径的文件路径加入队列
            sftpUtil.readPathToQueue(ftppath + "/" + DateUtil.date, sftp);
            //获取ftp连接下载文件到本地路径
            String ftpFilePath = null;
            while (!GlobalQueue.arrayBlockingQueue.isEmpty()) {
                //从队列中获取json格式的文件信息
                HashMap<String, String> strMap = gson.fromJson(GlobalQueue.arrayBlockingQueue.poll().toString(), HashMap.class);
                ftpFilePath = strMap.get("fileName");
                if (ftpFilePath != null) {
                    //下载文件到本地并发送kafka，同時刪除ftp上的数据
                    sendToKafka(ftpFilePath, sftp, localpath + "/" + DateUtil.getNowStr(), kafkaProducer);
                }
            }
            Thread.sleep(5000);
        } catch (Exception e) {
            logger.error("主程序执行入口方法失败,Method:{},error:{}", "start", e);
        }

    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取kafka生产者
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static Producer getKafkaProducer() {
        Properties properties = new Properties();
        properties.put("zookeeper.connect", ConfigUtil.getString("datacenter.unicom.zookper"));//声明zk
        properties.put("serializer.class", StringEncoder.class.getName());
        properties.put("metadata.broker.list", ConfigUtil.getString("datacenter.unicom.kafkahost"));// 声明kafka broker
        return new Producer<String, String>(new ProducerConfig(properties));
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：下载文件到本地并发送kafka，同時刪除ftp上的数据
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private static void sendToKafka(String ftpFilePath, ChannelSftp sftp, String localpath, Producer kafkaProducer) {
        try {
            //获取日期下的文件夹目录
            String nowFilePath = ftpFilePath.split(DateUtil.getNowStr())[1];
            String localFilePath = localpath + nowFilePath.substring(0, nowFilePath.lastIndexOf("/"));
            File localFile = new File(localFilePath);
            if (!localFile.exists()) {
                localFile.mkdirs();
            }

            //ftp上文件名
            String gzName = ftpFilePath.substring(ftpFilePath.lastIndexOf("/") + 1);
            sftpUtil.download(ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")), gzName, localFilePath + "/" + gzName, sftp);
            //下载文件到本地后删除远程ftp文件
            sftpUtil.delete(ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")), gzName, sftp);
            //读取文件发送到kafka
            File file = new File(localFilePath + "/" + gzName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file)), "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                boolean flag = false;//是否过滤标识位
                String[] split = line.split("\\|");

                //url数据中有空字段的数据不予处理
                if (split.length == 14) {
                    if (!split[13].trim().equals("")) {
                        flag = true;
                    }
                }

                if (flag) {
                    KeyedMessage<String, String> km = new KeyedMessage<String, String>(ConfigUtil.getString("datacenter.unicom.kafkatopic"), line + "|" + ftpFilePath);
                    kafkaProducer.send(km);
                }
            }
            file.delete();
        } catch (Exception e) {
            logger.error("下载文件到本地并发送kafka失败,Method:{},error:{}", "sendToKafka", e);
        }
    }

}
