package unicom.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.net.URI;

/**
 * Created by bjut_zjh on 2016/5/7.
 */
public class HdfsApp {
    private static Configuration conf = new Configuration();
    private static FileSystem fs;
    private static FSDataOutputStream hdfsOutStream;
    public static void main(String[] args) {
        try {
            conf.set("dfs.client.block.write.replace-datanode-on-failure.enable","true");
            conf.set("dfs.client.block.write.replace-datanode-on-failure.policy","NEVER");
            String pathUrl = "hdfs://master02:8020/unicom_url/part-000000.data";
            fs = FileSystem
                    .get(new URI("hdfs://master02:8020"), conf);
            if (!fs.exists(new Path(pathUrl))) {
                hdfsOutStream = fs.create(new Path(
                        pathUrl));
            }else{
                hdfsOutStream = fs.append(new Path(
                        pathUrl));
            }

            String comment = "aaaaa\n";
            hdfsOutStream.write(comment.getBytes());
            hdfsOutStream.close();
            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
