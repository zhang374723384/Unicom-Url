package com.unicom.storm;

import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.unicom.utils.ConfigUtil;
import com.unicom.utils.DateUtil;
import com.unicom.utils.HiveJDBCUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.fs.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.InetAddress;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：url过滤完毕后存储hdfs的bolt,会根据每条message后的路径信息存入不同hdfs路径
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class UrlEtlBolt extends BaseRichBolt {
    private static Logger logger = LoggerFactory.getLogger(UrlEtlBolt.class);
    private static HiveJDBCUtil hiveJDBCUtil = new HiveJDBCUtil();
    private OutputCollector collector;

    public void prepare(Map map, TopologyContext context,
                        OutputCollector collector) {
        this.collector = collector;
    }

    //本次bolt存储的顶级目录uuid
    String DIR_UUID = UUID.randomUUID().toString().replaceAll("-", "");
    //本地文件存储名中的UUID
    String FILE_UUID = UUID.randomUUID().toString().replaceAll("-", "");
    Date initDate = new Date();//bolt初始化时间

    public void execute(Tuple input) {
        Date operaDate = new Date();
        try {
            String str = input.getStringByField("url");
            //设置定时任务，本身是想使用storm的定时任务去做，到kafka-spout会产生未知bug因此自己实现
            if (((operaDate.getTime() - initDate.getTime()) / 1000) > Integer.parseInt(ConfigUtil.getString("datacenter.unicom.messageTiming"))) {
                //写数据文件到hive
                getFilesToHive(ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + DIR_UUID, DIR_UUID);
                FILE_UUID = UUID.randomUUID().toString().replaceAll("-", "");
                //初始化定时任务时间
                initDate = operaDate;
            }

            String[] split = str.split("\\|");
            String ftpFilePath = split[14];//url ftp路径
            String startTime = split[4];
            String urlStr = str.substring(0, str.lastIndexOf("|"));
            //获取分区所需的字段名
            String source = ConfigUtil.getString("datacenter.unicom.source");
            String province = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).substring(
                    ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
            ftpFilePath = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/"));
            String dateTime = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).substring(
                    ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
            //获取操作的小时
            Calendar cal = getCalendar(startTime, "yyyy-MM-dd hh:mm:ss");
            String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));

            cal = getCalendar(dateTime, "yyyyMMdd");
            String year = String.valueOf(cal.get(Calendar.YEAR));
            String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
            String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

            //本地该日省份url写入文件
            String partition = getPartition(year, month, day, hour, province, source);
            String fileDir = ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + DIR_UUID + "/" + partition;
            File localProvicenDir = new File(fileDir);
            if (!localProvicenDir.exists()) {
                localProvicenDir.mkdirs();
            }

            //写文件到本地文件
            File localprovince = new File(localProvicenDir.getAbsolutePath() + "/url-" + FILE_UUID + ".data");
            if (!localprovince.exists()) {
                InetAddress netAddress = InetAddress.getLocalHost();
                logger.info("localprovince netAddress::" + netAddress.getHostAddress());
                localprovince.createNewFile();
                toWriteLocal(localprovince, urlStr + "\n");
            } else {
                toWriteLocal(localprovince, urlStr + "\n");
            }
            this.collector.ack(input);
        } catch (Exception e) {
            this.collector.fail(input);
            e.printStackTrace();
            logger.error("[Class:UrlEtlBolt][Method:execute][Message:{}][Exception:{}]",
                    "UrlEtlBolt执行execute错误", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取hive的分区
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private String getPartition(String year, String month, String day, String hour, String province, String source) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(year + "/");
        stringBuffer.append(month + "/");
        stringBuffer.append(day + "/");
        stringBuffer.append(hour + "/");
        stringBuffer.append(province + "/");
        stringBuffer.append(source);
        return stringBuffer.toString();
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：日期格式化方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private Calendar getCalendar(String day, String formatStr) {
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
            Date parse = simpleDateFormat.parse(day);
            cal.setTime(parse);
        } catch (Exception e) {
            logger.error("", e);
        }
        return cal;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：将本地数据文件加载进hive表中
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    static void getFilesToHive(String filePath, String dir) {
        File root = new File(filePath);
        File[] files = root.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                getFilesToHive(file.getAbsolutePath(), dir);
            } else {
                String[] substring = filePath.replace(ConfigUtil.getString("datacenter.unicom.rootdir") + "/" + dir + "/", "").split("/");
                hiveJDBCUtil.executeHql("alter table unicom_url add if not exists partition (year='" + substring[0] + "',month='" + substring[1] + "',day='" + substring[2] + "',hour='" + substring[3] + "',province='" + substring[4] + "',source='" + substring[5] + "')");
                toWriteHdfs(new File(file.getAbsolutePath()), substring);
            }
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：写文件到hdfs
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    static void toWriteHdfs(File localprovince, String[] substring) {
        try {
            logger.info("localprovince::" + localprovince);
            InputStream in = new BufferedInputStream(new FileInputStream(localprovince));
            Path path = new Path(ConfigUtil.getString("datacenter.unicom.HdfsPath") + "/" + "year=" + substring[0] + "/month=" + substring[1] + "/day=" + substring[2] + "/hour=" + substring[3] + "/province=" + substring[4] + "/source=" + substring[5] + "/" + "url-" + UUID.randomUUID().toString().replaceAll("-", "") + ".data");
            Configuration conf = new Configuration();
            FileSystem fileSystem = FileSystem.get(new URI(
                    ConfigUtil.getString("datacenter.unicom.hdfs")), conf);
            FSDataOutputStream out = fileSystem.create(path);
            IOUtils.copyBytes(in, out, 4096, true);
            localprovince.delete();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("[Class:UrlEtlBolt][Method:toWriteHdfs][Message:{}][Exception:{}]",
                    "本地文件写入hdfs错误", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取url数据写入本地路径
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void toWriteLocal(File file, String conent) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file, true)));
            out.write(conent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                logger.error("[Class:UrlEtlBolt][Method:toWriteLocal][Message:{}][Exception:{}]",
                        "过滤数据写入本地文件错误", e);
            }
        }
    }
}
