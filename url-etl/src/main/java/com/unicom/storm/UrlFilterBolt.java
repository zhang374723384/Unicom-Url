package com.unicom.storm;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.unicom.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：storm的url的过滤bolt，去除不符合要求的url
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class UrlFilterBolt extends BaseRichBolt {
    private static Logger logger = LoggerFactory.getLogger(UrlFilterBolt.class);
    private OutputCollector collector;
    private static String UUID_STR = UUID.randomUUID().toString();

    public void prepare(Map map, TopologyContext context,
                        OutputCollector collector) {
        this.collector = collector;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取一天url数据并过滤有空字段的url
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void execute(Tuple input) {
        try {
            boolean flag = true;//是否过滤标识位
            String str = input.getString(0);
            String[] split = str.split("\\|");

            //url数据中有空字段的数据不予处理
            if (split.length != 15) {
                flag = false;
            }

            //符合数据标准并处理
            if (flag) {
                // 正则提取网址
                String url = split[13];
                String regexUrl = getRegexUrl(url);
                str = str.replace(url, regexUrl);
                this.collector.emit(input, new Values(str));
                this.collector.ack(input);
            }
        } catch (Exception e) {
            this.collector.fail(input);
            logger.error("获取url并过滤失败,{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：url正则截取方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private String getRegexUrl(String url) {
        Pattern pattern = Pattern
                .compile("(http://|ftp://|https://|www|){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)");
        // 空格结束
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return url;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url"));
    }
}
