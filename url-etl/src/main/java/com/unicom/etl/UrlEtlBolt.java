package com.unicom.etl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.unicom.utils.ConfigUtil;
import com.unicom.utils.DateUtil;
import com.unicom.utils.HiveJDBCUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：storm的url的过滤bolt，去除不符合要求的url
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class UrlEtlBolt extends BaseRichBolt {
    private static Logger logger = LoggerFactory.getLogger(UrlEtlBolt.class);
    private static HiveJDBCUtil hiveJDBCUtil = new HiveJDBCUtil();
    private static Configuration conf = new Configuration();
    private OutputCollector collector;

    public void prepare(Map map, TopologyContext context,
                        OutputCollector collector) {
        this.collector = collector;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取一天url数据并过滤有空字段的url
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public void execute(Tuple input) {
        try {
            boolean flag = true;//是否过滤标识位
            String str = input.getString(0);
            String[] split = str.split("\\|");

            //url数据中有空字段的数据不予处理
            if (split.length != 15) {
                flag = false;
            }

            //符合数据标准并处理
            if (flag) {
                // 正则提取网址
                String url = split[13];
                String regexUrl = getRegexUrl(url);
                str = str.replace(url, regexUrl);

                //写入hdfs加载进hive分区表
                getUrlToHiveHdfs(str);
                this.collector.ack(input);
            }
        } catch (Exception e) {
            this.collector.fail(input);
            logger.error("获取url并过滤失败,{}", e);
        }
    }

    private void getUrlToHiveHdfs(String str) {
        String[] split = str.split("\\|");
        String ftpFilePath = split[14];//url ftp路径
        String startTime = split[4];
        String urlStr = str.substring(0, str.lastIndexOf("|"));
        //获取分区所需的字段名
        String source = ConfigUtil.getString("datacenter.unicom.source");
        String province = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).substring(
                ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
        ftpFilePath = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/"));
        String dateTime = ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).substring(
                ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")).lastIndexOf("/") + 1);
        //获取操作的小时
        Calendar cal = getCalendar(startTime, "yyyy-MM-dd hh:mm:ss");
        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));

        cal = getCalendar(dateTime, "yyyyMMdd");
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

        //本地该日省份url写入文件
        String partition = getPartition(year, month, day, hour, province, source);
        String[] substring = partition.split("/");
        hiveJDBCUtil.executeHql("alter table unicom_url add if not exists partition (year='" + substring[0] + "',month='" + substring[1] + "',day='" + substring[2] + "',hour='" + substring[3] + "',province='" + substring[4] + "',source='" + substring[5] + "')");
        toWriteHdfs(partition, urlStr);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    private void toWriteHdfs(String partition, String urlStr) {
        try {
            FSDataOutputStream hdfsOutStream;
            conf.setBoolean("dfs.support.append", true);
            conf.set("dfs.client.block.write.replace-datanode-on-failure.enable", "true");
            conf.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");
            conf.set("fs.default.name", "hdfs://master02:8020");
            String pathUrl = ConfigUtil.getString("datacenter.unicom.HdfsPath") + "/" + getHdfsPath(partition);
            FileSystem fs = FileSystem
                    .get(new URI(ConfigUtil.getString("datacenter.unicom.hdfs")), conf);
//            if (!fs.exists(new Path(pathUrl))) {
//                fs.create(new Path(
//                        pathUrl));
//                fs.close();
//                fs = FileSystem
//                        .get(new URI(ConfigUtil.getString("datacenter.unicom.hdfs")), conf);
//            }
//            FSDataOutputStream hdfsOutStream = fs.append(new Path(
//                    pathUrl));

            if (!fs.exists(new Path(pathUrl))) {
                hdfsOutStream = fs.create(new Path(
                        pathUrl));
            }else{
                hdfsOutStream = fs.append(new Path(
                        pathUrl));
            }
            String comment = urlStr + "\n";
            hdfsOutStream.write(comment.getBytes());
            hdfsOutStream.close();
            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getHdfsPath(String partition) {
        String[] substring = partition.split("/");
        return "year=" + substring[0] + "/month=" + substring[1] + "/day=" + substring[2] + "/hour=" + substring[3] + "/province=" + substring[4] + "/source=" + substring[5] + "/part-000000.data";
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：url正则截取方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private String getRegexUrl(String url) {
        Pattern pattern = Pattern
                .compile("(http://|ftp://|https://|www|){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)");
        // 空格结束
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return url;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取hive的分区
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private String getPartition(String year, String month, String day, String hour, String province, String source) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(year + "/");
        stringBuffer.append(month + "/");
        stringBuffer.append(day + "/");
        stringBuffer.append(hour + "/");
        stringBuffer.append(province + "/");
        stringBuffer.append(source);
        return stringBuffer.toString();
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：日期格式化方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    private Calendar getCalendar(String day, String formatStr) {
        Calendar cal = Calendar.getInstance();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
            Date parse = simpleDateFormat.parse(day);
            cal.setTime(parse);
        } catch (Exception e) {
            logger.error("", e);
        }
        return cal;
    }
}
