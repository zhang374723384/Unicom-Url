package com.unicom.etl;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import com.unicom.utils.ConfigUtil;
import storm.kafka.*;

import java.util.Arrays;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：url过滤主程序，定义kafka-spout及urlguolv存储bolt，从kafka-spout读取url后过滤写入hdfs
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class StormToHdfsEtl {

    public static void main(String[] args) throws AlreadyAliveException,
            InvalidTopologyException, InterruptedException {
        String zks = ConfigUtil.getString("datacenter.unicom.zookper");
        String topic = ConfigUtil.getString("datacenter.unicom.kafkatopic");
        String zkRoot = ConfigUtil.getString("datacenter.unicom.zkRoot");
        String id = "StormKafka_url";

        BrokerHosts brokerHosts = new ZkHosts(zks);
        SpoutConfig spoutConf = new SpoutConfig(brokerHosts, topic, zkRoot, id);
        spoutConf.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConf.forceFromStart = true;
        spoutConf.zkServers = Arrays.asList(ConfigUtil.getString("datacenter.unicom.zkServers").split(","));
        spoutConf.zkPort = 2181;

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("Kafka-Reader", new KafkaSpout(spoutConf), 3); // Kafka我们创建了一个3分区的Topic，这里并行度设置为3
        builder.setBolt("Url-Etl", new UrlEtlBolt(), 5).shuffleGrouping(
                "Kafka-Reader");

        Config conf = new Config();
        conf.put(Config.STORM_ZOOKEEPER_SESSION_TIMEOUT, Integer.parseInt(ConfigUtil.getString("datacenter.unicom.zkTimeOut")));
        conf.put(Config.STORM_ZOOKEEPER_CONNECTION_TIMEOUT, Integer.parseInt(ConfigUtil.getString("datacenter.unicom.zkConTimeOut")));
        conf.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, Integer.parseInt(ConfigUtil.getString("datacenter.unicom.messageTimeOut")));

        String name = StormToHdfsEtl.class.getSimpleName();
        if (args != null && args.length > 0) {
            conf.put(Config.NIMBUS_HOST, args[0]);
            conf.setNumWorkers(5);
            StormSubmitter.submitTopologyWithProgressBar(name, conf,
                    builder.createTopology());
        } else {
//            conf.setNumWorkers(5);
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology(name, conf, builder.createTopology());
        }
    }
}