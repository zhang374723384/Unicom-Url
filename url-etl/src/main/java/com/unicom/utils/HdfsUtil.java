package com.unicom.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：hdfs写入文件工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class HdfsUtil {
    private static Logger logger = LoggerFactory.getLogger(HdfsUtil.class);
    private static Configuration conf = new Configuration();
    private static FileSystem fs;
    private static FSDataOutputStream hdfsOutStream;

    static {
        try {
            String pathUrl = ConfigUtil.getString("datacenter.black_roll.hdfsPath");
            fs = FileSystem
                    .get(new URI(ConfigUtil.getString("datacenter.black_roll.hdfs")), conf);
            if (!fs.exists(new Path(pathUrl))) {
                hdfsOutStream = fs.create(new Path(
                        pathUrl));
            }
            hdfsOutStream = fs.append(new Path(
                    pathUrl));
        } catch (Exception e) {
            logger.error("[Class:HdfsUtil][Method:init][Message:{}][Exception:{}]",
                    "hdfs初始化失败", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：hdfs向文件续写方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static boolean toAddHdfsHive(String line) {
        try {
            String comment = line + "\n";
            hdfsOutStream.write(comment.getBytes());
            hdfsOutStream.close();
            fs.close();
        } catch (Exception e) {
            logger.error("[Class:HdfsUtil][Method:toAddHdfsHive][Message:{}][Exception:{}]",
                    "hdfs插入数据失败", e);
        }

        return true;
    }

    public static void main(String[] args) {
        HdfsUtil.toAddHdfsHive("zhangjinghui!!");
    }
}
