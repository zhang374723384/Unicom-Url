package com.unicom.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.unicom.utils.ConfigUtil;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：hive的jdbc工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class HiveJDBCUtil {
    private static Logger logger = LoggerFactory.getLogger(HiveJDBCUtil.class);

    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    private String db_url = "";
    private String db_user = "";
    private String db_passwd = "";

    public Connection getConnection() {
        db_url = ConfigUtil.getString("datacenter.unicom.hive_url");
        db_user = ConfigUtil.getString("datacenter.unicom.hive_user");
        try {
            db_passwd = ConfigUtil.getString("datacenter.unicom.hive_passwd");
        } catch (Exception e) {
            logger.error("[Class:HiveJDBCConnection][Method:init][Message:{}][url:{}][Exception:{}]",
                    "初始化Hive连接信息时，解密密码失败", db_url, e);
            throw new RuntimeException(String.format("[Class:HiveJDBCConnection][Method:init][Message:{}][url:{}][Exception:{}]",
                    "初始化Hive连接信息时，解密密码失败", db_url, e));
        }

        Connection con = null;
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(db_url, "", "");
        } catch (ClassNotFoundException e) {
            logger.error("[Class:HiveJDBCConnection][Method:getConnection][Message:{}][url:{}][Exception:{}]",
                    "HiveJDBCConnection连接创建失败", db_url, e);
            throw new RuntimeException(String.format("[Class:HiveJDBCConnection][Method:getConnection][Message:{}][url:{}][Exception:{}]",
                    "HiveJDBCConnection连接创建失败", db_url, e));
        } catch (SQLException e) {
            logger.error("[Class:HiveJDBCConnection][Method:getConnection][Message:{}][url:{}][Exception:{}]",
                    "HiveJDBCConnection连接创建失败", db_url, e);
            throw new RuntimeException(String.format("[Class:HiveJDBCConnection][Method:getConnection][Message:{}][url:{}][Exception:{}]",
                    "HiveJDBCConnection连接创建失败", db_url, e));
        }
        return con;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：hive执行插入语句并返回结果方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public List<Map<String, Object>> execute(String sql) {
        Connection con = null;
        Statement stmt = null;
        ResultSet res = null;
        List<Map<String, Object>> list = null;
        try {
            con = getConnection();
            stmt = con.createStatement();
            res = stmt.executeQuery(sql);
            Map<String, Object> map = null;
            ResultSetMetaData rsmd = res.getMetaData();
            int rowCnt = rsmd.getColumnCount();
            list = new LinkedList<Map<String, Object>>();
            while (res.next()) {
                map = new LinkedHashMap<String, Object>(rowCnt);
                for (int i = 1; i <= rowCnt; i++) {
                    map.put(rsmd.getColumnName(i), res.getObject(i));
                }
                list.add(map);
            }
        } catch (Exception e) {
            logger.error("[Class:HiveJDBCConnection][Method:execute][Message:{}][sql:{}][Exception:{}]",
                    "Hive sql error", sql, e);
            throw new RuntimeException(String.format("[Class:HiveJDBCConnection][Method:execute][Message:{}][sql:{}][Exception:{}]",
                    "Hive sql error", sql, e));
        } finally {
            try {
                if (null != stmt) {
                    stmt.close();
                }
                if (null != con) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：hive执行定义语句方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public boolean executeHql(String sql) {
        Connection con = null;
        Statement stmt = null;
        try {
            con = getConnection();
            stmt = con.createStatement();
            boolean execute = stmt.execute(sql);
            return execute;
        } catch (Exception e) {
            logger.error("[Class:HiveJDBCConnection][Method:executeHql][Message:{}][sql:{}][Exception:{}]",
                    "Hive sql error", sql, e);
            throw new RuntimeException(String.format("[Class:HiveJDBCConnection][Method:execute][Message:{}][sql:{}][Exception:{}]",
                    "Hive sql error", sql, e));
        } finally {
            try {
                if (null != stmt) {
                    stmt.close();
                }
                if (null != con) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static String getExpireDate(java.util.Date expiredate) {
        if (expiredate != null) {
            return "|" + expiredate.toLocaleString().substring(0, 10).replace("-", "");
        }
        return "";
    }

//    public static void main(String[] args) {
//        HiveJDBCUtil hiveJDBCUtil = new HiveJDBCUtil();
//        List<Map<String, Object>> execute = hiveJDBCUtil.execute("show partitions unicom_logs");
//        for(Map < String, Object > map:execute){
//            System.out.println(map.toString());
//        }
//    }
}
