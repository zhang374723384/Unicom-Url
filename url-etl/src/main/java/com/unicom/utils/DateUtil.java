package com.unicom.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：日期相关方法工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class DateUtil {
    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    public static void toSleep() {
        try {
            Thread.sleep(1000 * 15);
        } catch (InterruptedException e) {
            logger.error("[Class:DateUtil][Method:toSleep][Message:{}][error:{}]",
                    "Thread.sleep error", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取地址格式当前日期的方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static String getDateStr() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();
        String[] dateSplit = date.toLocaleString().split(" ")[0].split("-");
        String dateStr = dateSplit[0].substring(2) + (dateSplit[1].length() == 1 ? "0" + dateSplit[1] : dateSplit[1]) + dateSplit[2];
        return dateStr;
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月5日
     * 开发内容：获取地址格式当前日期的方法
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static String getNowStr() {
        String date = new java.text.SimpleDateFormat("yyyyMMdd").format(new Date());
        return date;
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getNowStr());
    }
}
