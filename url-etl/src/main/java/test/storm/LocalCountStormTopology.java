package test.storm;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import backtype.storm.Constants;
import backtype.storm.StormSubmitter;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

/**
 * Created by bjut_zjh on 2016/4/12.
 */
public class LocalCountStormTopology {

    public static class DataSourceSpout extends BaseRichSpout {
        private Map conf;
        private TopologyContext context;
        private SpoutOutputCollector collector;

        public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
            this.conf = conf;
            this.context = context;
            this.collector = collector;
        }

        int messgaeid = 1;//消息id

        public void nextTuple() {
            this.collector.emit(new Values("我	是	中	国	人"), messgaeid);
            messgaeid++;
            Utils.sleep(2000);
        }

        @Override
        public void ack(Object msgId) {
            System.out.println("ack被调用了：" + msgId);
        }

        @Override
        public void fail(Object msgId) {
            System.out.println("fail被调用了：" + msgId);
        }

        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("line"));
        }
    }

    public static class SplitBolt extends BaseRichBolt {

        private Map stormConf;
        private TopologyContext context;
        private OutputCollector collector;

        /**
         * 初始化方法
         */
        public void prepare(Map stormConf, TopologyContext context,
                            OutputCollector collector) {
            this.stormConf = stormConf;
            this.context = context;
            this.collector = collector;
        }

        /**
         * 死循环
         */
        public void execute(Tuple input) {
            try {
                String line = input.getStringByField("line");
                String[] split = line.split("\t");
                for (String word : split) {
                    this.collector.emit(input, new Values(word));
                    //这个时候相当于吧word所在的tuple和上一级tuple绑定到一块了，
                }
                this.collector.ack(input);
            } catch (Exception e) {
                this.collector.fail(input);
            }

        }

        /**
         * 声明输出字段
         * 注意：在nexttuple或者execute方法中 如果没有向外发射tuple，那么在这就不需要声明输出字段
         */
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word"));
        }
    }


    public static class CountBolt extends BaseRichBolt {

        private Map stormConf;
        private TopologyContext context;
        private OutputCollector collector;

        /**
         * 初始化方法
         */
        public void prepare(Map stormConf, TopologyContext context,
                            OutputCollector collector) {
            this.stormConf = stormConf;
            this.context = context;
            this.collector = collector;
        }

        /**
         * 死循环
         */
        int num = 0;

        public void execute(Tuple input) {
            if (input.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)) {
                System.err.println("num::" + num);
            } else {
                String string = input.getString(0);
                num++;
                System.err.println("num++::" + num + string);
            }
        }

        public Map<String, Object> getComponentConfiguration() {
            Map<String, Object> conf = new HashMap<String, Object>();
            conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 2);
            return conf;
        }

        /**
         * 声明输出字段
         * 注意：在nexttuple或者execute方法中 如果没有向外发射tuple，那么在这就不需要声明输出字段
         */
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
        }
    }

    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
        //组装topology
        TopologyBuilder topologyBuilder = new TopologyBuilder();
        topologyBuilder.setSpout("aa", new DataSourceSpout());
        topologyBuilder.setBolt("bb", new SplitBolt()).shuffleGrouping("aa");
        topologyBuilder.setBolt("cc", new CountBolt(), 1).shuffleGrouping("bb");


        //使用本机storm集群运行topology
        LocalCluster localCluster = new LocalCluster();
        Config config = new Config();
        config.setNumWorkers(2);
        config.setNumAckers(0);
//        config.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 30);//
        localCluster.submitTopology("localTopology", config, topologyBuilder.createTopology());

//        StormSubmitter.submitTopologyWithProgressBar("localTopology", config,
//                topologyBuilder.createTopology());
    }
}
