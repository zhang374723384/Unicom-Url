package com;

import com.unicom.utils.ConfigUtil;
import com.unicom.utils.DateUtil;
import com.unicom.utils.HiveJDBCUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by bjut_zjh on 2016/4/14.
 */
public class test {
    private static Logger logger = LoggerFactory.getLogger(test.class);
    private static ArrayList<String> filelist = new ArrayList<String>();
    private static HiveJDBCUtil hiveJDBCUtil = new HiveJDBCUtil();
    private static Configuration conf = new Configuration();
    private static FileSystem fs;
    private static FSDataOutputStream hdfsOutStream;

    public static void main(String[] args) throws Exception {
        try {
            conf.set("dfs.client.block.write.replace-datanode-on-failure.enable","true");
            conf.set("dfs.client.block.write.replace-datanode-on-failure.policy","NEVER");
            String pathUrl = "hdfs://192.168.30.133:9000/url.data";
            fs = FileSystem
                    .get(new URI("hdfs://192.168.30.133:9000"), conf);
            if (!fs.exists(new Path(pathUrl))) {
                hdfsOutStream = fs.create(new Path(
                        pathUrl));
            }else{
                hdfsOutStream = fs.append(new Path(
                        pathUrl));
            }

            String comment = "aaaaa\n";
            hdfsOutStream.write(comment.getBytes());
            hdfsOutStream.close();
            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
