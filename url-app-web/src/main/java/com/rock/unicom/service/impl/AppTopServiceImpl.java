package com.rock.unicom.service.impl;

import com.rock.unicom.dao.AppTopMapper;
import com.rock.unicom.model.AppTop;
import com.rock.unicom.service.AppTopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：service获取app排行实现类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
@Service
@Transactional
public class AppTopServiceImpl implements AppTopService {
    @Autowired
    private AppTopMapper appTopMapper;

    @Override
    public List<AppTop> selectAppTop() {
        return appTopMapper.selectAppTop();
    }
}
