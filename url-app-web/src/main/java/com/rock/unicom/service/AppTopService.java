package com.rock.unicom.service;

import com.rock.unicom.model.AppTop;

import java.util.List;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：service获取app排行接口类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public interface AppTopService {
    List<AppTop> selectAppTop();
}
