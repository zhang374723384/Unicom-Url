package com.rock.unicom.dao;

import com.rock.unicom.model.AppTop;

import java.util.List;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：mapper获取app排名接口
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public interface AppTopMapper {
    List<AppTop> selectAppTop();
}
