package com.rock.unicom.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：app排名对象实体类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class AppTop implements Serializable {
    private String app_name;
    private Integer app_num;
    private String app_icon;

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Integer getApp_num() {
        return app_num;
    }

    public void setApp_num(Integer app_num) {
        this.app_num = app_num;
    }

    public String getApp_icon() {
        return app_icon;
    }

    public void setApp_icon(String app_icon) {
        this.app_icon = app_icon;
    }

    @Override
    public String toString() {
        return "AppTop{" +
                "app_name='" + app_name + '\'' +
                ", app_num=" + app_num +
                ", app_icon='" + app_icon + '\'' +
                '}';
    }
}
