package com.rock.unicom.web.controller;

import com.rock.unicom.model.AppTop;
import com.rock.unicom.service.AppTopService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月5日
 * 开发内容：app排行的后台action，包含页面跳转及获取app排行集合转化json返回页面方法
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
@Controller
public class AppTopWebController {

    @Autowired
    private AppTopService appTopService;

    @RequestMapping("appTopWeb")
    public String go_idcardQuery() {
        return "appTopWeb";
    }

    @ResponseBody
    @RequestMapping(value = "appTopList", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    public String getAppTopList() {
        JSONArray jsonArray = new JSONArray();
        List<AppTop> appTops = this.appTopService.selectAppTop();
        for (AppTop appTop : appTops) {
            jsonArray.put(new JSONObject(appTop));
        }
        return jsonArray.toString();
    }
}


