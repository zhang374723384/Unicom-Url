<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <%
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    %>

    <title>智慧足迹大数据app排行榜</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link href="<%=basePath %>/js/bootstrap.min14ed.css" rel="stylesheet">
    <link href="<%=basePath %>/js/font-awesome.min93e3.css" rel="stylesheet">

    <link href="<%=basePath %>/js/animate.min.css" rel="stylesheet">
    <link href="<%=basePath %>/js/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->
    <script>if (window.top !== window.self) {
        window.top.location = window.location;
    }</script>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div id="app_top">

    </div>
</div>
<script src="<%=basePath %>/js/jquery.min.js"></script>
<script src="<%=basePath %>/js/bootstrap.min.js"></script>
<script>
    window.onload = function () {
        $.ajax({
            type: "POST",
            async: true,
            url: "appTopList",
            success: function (data) {
                console.info(data);
                data = $.parseJSON(data);

                //组织排行榜表单
                var tabStr = "<table><tr align='center'><td width='150' height='60'><font style='font-weight:bold;font-size: 1.5em;'>排名</font></td><td width='150' height='60'><font style='font-weight:bold;font-size: 1.5em;'>图标</font></td><td width='100' height='60'><font style='font-weight:bold;font-size: 1.5em;'>名称</font></td></tr>";

                for (var i = 0; i < 100; i++) {
                    tabStr += "<tr align='center'><td width='150' height='40'><font style='font-weight:bold;font-size: 1.1em;'>" + (i + 1) + "</font></td><td width='150' height='40'><img src='<%=basePath %>/js/images/" + data[i].app_icon + ".png' height='35' width='35'></td><td width='150' height='40'><font style='font-weight:bold;font-size: 1.1em;'>" + data[i].app_name + "</font></td></tr>";
                }
                tabStr += "</table>";
                $("#app_top").html(tabStr);
            }
        });
    }
</script>
</body>
</html>