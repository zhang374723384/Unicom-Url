package com.unicom;


import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月3日
 * 开发内容：mysql连接及语句执行工具类
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class MysqlUtil {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(MysqlUtil.class);
    private static Connection conn;
    private static Map<String, String> resultMap = new HashMap<String, String>();
    //驱动程序名
    private static String driver = "com.mysql.jdbc.Driver";
    //URL指向要访问的数据库名mydata
    private static String url = ConfigUtil.getString("datacenter.unicom.mysql.url");
    //MySQL配置时的用户名
    private static String user = ConfigUtil.getString("datacenter.unicom.mysql.username");
    //MySQL配置时的密码
    private static String password = ConfigUtil.getString("datacenter.unicom.mysql.password");

    static {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            logger.error("获取mysql链接失败:{}", e);
        }
    }

    /**
     * 开发人：张京辉
     * 开发时间：2016年5月4日
     * 开发内容：获取app关键字对应关系
     * 修改人：
     * 修改时间：
     * 修改内容：
     */
    public static Map<String, String> toAddRecord() {
        try {
            PreparedStatement preparedStatement = MysqlUtil.conn.prepareStatement("SELECT keyword,appname FROM app_keyword");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                resultMap.put(resultSet.getString(1), resultSet.getString(2));
            }
        } catch (SQLException e) {
            logger.error("向mysql添加下载记录失败:{}", e);
        }
        return resultMap;
    }
}
