package com.unicom;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;

/**
 * 开发人：张京辉
 * 开发时间：2016年5月4日
 * 开发内容：根据url对app进行对应的mapreduce
 * 修改人：
 * 修改时间：
 * 修改内容：
 */
public class AppTopMapReduce extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new AppTopMapReduce(), args);
        System.exit(exitCode);
    }

    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();
        //获取结果输出路径
        Path outPath = new Path(ConfigUtil.getString("datacenter.unicom.output.top.path"));
        FileSystem fs = null;

        fs = FileSystem.get(new URI(
                        ConfigUtil.getString("datacenter.unicom.hdfs")),
                conf);
        //判断hdfs存储路径是否存在，存在就删除
        if (fs.exists(outPath)) {
            fs.delete(outPath);
        }

        Job job = Job.getInstance(conf, AppTopMapReduce.class.getSimpleName());
        job.setJarByClass(AppTopMapReduce.class);
        job.setMapperClass(MyMap.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        //设置mapreduce数据源输入路径
        FileInputFormat.setInputPaths(job, ConfigUtil.getString("datacenter.unicom.input.top.path"));
        FileOutputFormat.setOutputPath(job, outPath);
        return (job.waitForCompletion(true) ? 0 : 1);
    }

    public static class MyMap extends
            Mapper<LongWritable, Text, Text, Text> {
        public static java.util.Map<String, String> appMap = MysqlUtil.toAddRecord();
        Text url_num = new Text();
        Text url = new Text();
        Text appName = new Text("-");

        protected void map(
                LongWritable key,
                Text value,
                Context context)
                throws IOException, InterruptedException {
            //获取求出的imei对应url
            String[] split = value.toString().split("\t");
            url.set(split[0]);
            url_num.set(split[1]);

            String app_name = "-";
            //遍历app关键字确定
            for (String keyword : appMap.keySet()) {
                if (url.toString().contains(keyword)) {
                    appName.set(appMap.get(keyword));
                    break;
                }
            }

            context.write(appName, url_num);
        }
    }
}

