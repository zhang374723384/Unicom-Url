#!/usr/bin/env bash

echo "url top is start"
weekday=`date +%w`
sunday=`date -d 'sunday 1 week ago' +%Y%m%d`
if [[ weekday -eq 1 ]] ; then
monday=`date -d 'monday 1 week ago' +%Y%m%d`
else
monday=`date -d 'monday 2 week ago' +%Y%m%d`
fi

echo "sunday: $sunday"
echo "monday: $monday"

tempdate=`date -d "-0 day $sunday" +%F`
enddate=`date -d "-0 day $monday" +%F`
tempdateSec=`date -d "-0 day $sunday" +%s`
enddateSec=`date -d "-0 day $monday" +%s`

echo "##################################################"
hive -e "use unicom;drop table if exists url_top_middle;create table url_top_middle (imei string,url string);"

for i in `seq 1 10`; do
  if [[ $tempdateSec -lt $enddateSec ]]; then
    break
  fi

  year=${tempdate:0:4}
  if [[ ${tempdate:5:1} == "0" ]] ; then
  month=${tempdate:6:1}
  else
  month=${tempdate:5:2}
  fi
  if [[ ${tempdate:8:1} == "0" ]] ; then
  day=${tempdate:9:1}
  else
  day=${tempdate:8:2}
  fi
  echo $year-$month-$day
  
  echo "hive sql is start"
  
  hive -e "use unicom;insert into table url_top_middle select imei,url from unicom_url where year='$year' and month='$month' and day='$day' and imei<>'' and imei is not null group by imei,url;"
  
  tempdate=`date -d "-$i day $sunday" +%F`
  tempdateSec=`date -d "-$i day $sunday" +%s`
done

hive -e "use unicom;drop table if exists url_num_middle;create table url_num_middle as select a.url,count(1) as url_num from (select imei,url from url_top_middle group by imei,url) as a group by a.url;"
hive -e "use unicom;drop table if exists url_num;create table url_num ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' as select url,sum(url_num) as url_num from url_num_middle group by url;"
hive -e "use unicom;drop table if exists app_top_mapreduce;create table app_top_mapreduce (app_name string,app_num int) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t';"

hadoop jar /home/azkaban/azkaban-executor-2.5.0/executions/user_app/app-top.jar com.unicom.AppTopMapReduce

hive -e "use unicom;drop table if exists app_top;create table app_top ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' as select app_name,sum(app_num) as app_num from app_top_mapreduce where app_name<>'-' group by app_name sort by app_num desc limit 100;"

echo "sqoop export app_top"
mysql -uroot -p123456 -e"use unicom;truncate table app_top;" 
sqoop export --connect jdbc:mysql://MASTER02:3306/unicom --username root --password 123456 --table app_top --export-dir /user/hive/warehouse/unicom.db/app_top --fields-terminated-by '\t' --input-null-non-string '\\N' --input-null-string '\\N'
echo "url top is end"