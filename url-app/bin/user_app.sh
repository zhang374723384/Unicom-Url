#!/usr/bin/env bash

echo "url app is start"
weekday=`date +%w`
sunday=`date -d 'sunday 1 week ago' +%Y%m%d`
if [[ weekday -eq 1 ]] ; then
monday=`date -d 'monday 1 week ago' +%Y%m%d`
else
monday=`date -d 'monday 2 week ago' +%Y%m%d`
fi

echo "sunday: $sunday"
echo "monday: $monday"

tempdate=`date -d "-0 day $sunday" +%F`
enddate=`date -d "-0 day $monday" +%F`
tempdateSec=`date -d "-0 day $sunday" +%s`
enddateSec=`date -d "-0 day $monday" +%s`

echo "##################################################"
hive -e "use unicom;drop table if exists user_url;create table user_url (imei string,url string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/unicom_url/user_url' ;"

for i in `seq 1 10`; do
  if [[ $tempdateSec -lt $enddateSec ]]; then
    break
  fi

  year=${tempdate:0:4}
  if [[ ${tempdate:5:1} == "0" ]] ; then
  month=${tempdate:6:1}
  else
  month=${tempdate:5:2}
  fi
  if [[ ${tempdate:8:1} == "0" ]] ; then
  day=${tempdate:9:1}
  else
  day=${tempdate:8:2}
  fi
echo $year-$month-$day
  
echo "hive sql is start"
  
  hive -e "use unicom;insert into table user_url select imei,url from unicom_url where year='$year' and month='$month' and day='$day' group by imei,url;"
  
  tempdate=`date -d "-$i day $sunday" +%F`
  tempdateSec=`date -d "-$i day $sunday" +%s`
done

hive -e "use unicom;drop table if exists user_url_uniqueness;create table user_url_uniqueness (imei string,url string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/unicom_url/user_url_uniqueness';"
hive -e "use unicom;insert into table user_url_uniqueness select imei,url from user_url group by imei,url"
hive -e "use unicom;drop table if exists user_app; create table user_app (imei string,appname string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/unicom_url/user_app';"

echo "mapreduce is start"
hadoop jar /home/azkaban/azkaban-executor-2.5.0/executions/user_app/app-1.0-SNAPSHOT.jar com.unicom.UserAppMapReduce
echo "mapreduce is end"

hive -e "use unicom;drop table if exists user_app_result;create table user_app_result ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/unicom_url/user_app_result' as select imei,appname from user_app where imei<>'' and imei is not null and appname<>'-' group by imei,appname sort by imei;"

echo "hive sql is end"
echo "url app is end"